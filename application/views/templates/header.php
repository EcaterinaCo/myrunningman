<!doctype html>
<html lang="en">
<head>
   <!-- Required meta tags -->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   <!-- Bootstrap CSS -->
   <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->
   <link rel="stylesheet" href="/assets/css/bootstrap.min.css">

   <!-- Icons -->
   <link rel="stylesheet" href="/assets/css/faall.min.css">

   <!-- Lightbox -->
   <link rel="stylesheet" href="/assets/css/baguetteBox.min.css">
   <link rel="stylesheet" href="/assets/css/grid-gallery.css">

   <!-- Main Style -->
   <link href="/assets/css/style.css" rel="stylesheet">

   <title><?= $title; ?></title>

</head>
<body>

   <!-- Header -->
   <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
      <div class="container">

         <!-- Элементы, остающиеся в header'e при разрешении <=768 --> 
         <a class="navbar-brand" href="#">
            <img src="/assets/img/banner1.png" alt="Running man">
         </a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
         </button>

         <!-- Элементы, уходящие в гамбургер  -->
         <div class="collapse navbar-collapse" id="navbarCollapse">
            <!-- Меню -->
            <ul class="navbar-nav col-sm-8">
               <li class="nav-item">
                  <a class="nav-link active" href="#">Серии</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#">Новости</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#">Альбом</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#">Участники</a>
               </li>
            </ul>

            <!-- Поиск и профиль -->
            <div class="d-flex align-items-center">
               <form class="form-search">
                  <div class="input-group">
                     <input type="search" class="form-control" placeholder="Поиск...">
                     <div class="input-group-append">
                        <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i></button>
                     </div>
                  </div>
               </form>
               <a class="profile btn btn-link" href="#">
                  <i class="fas fa-user"></i>
               </a>
            </div>
         </div>
         <!-- Конец гамбургера -->

      </div>
   </nav>
   <!-- Header END -->